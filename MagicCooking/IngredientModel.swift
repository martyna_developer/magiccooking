//
//  IngredientModel.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 07/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class IngredientModel {
    
    var ingredientName: Ingredient?
    var ingredientImage: UIImage?
    
    init(ingredientName: Ingredient? = nil, ingredientImage: UIImage? = nil) {
       
        self.ingredientName = ingredientName
        self.ingredientImage = ingredientImage
    }
    
}
