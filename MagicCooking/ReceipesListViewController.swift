//
//  ReceipesListViewController.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 02/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class ReceipesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private enum Constants {
        static let recipesDetailsSegueId = "showReceipeDetails"
    }
    
    private lazy var receipesList = ReceipesListModel()
    private lazy var chosenReceipes = ReceipesListModel()
    
    var chosenIngredient = IngredientModel()
    var chosenReceipe = ReceipeModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.title = (chosenIngredient.ingredientName?.rawValue.first?.uppercased() ?? "") + String(chosenIngredient.ingredientName?.rawValue.dropFirst() ?? "")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        matchReceipesWithChosenIngredient()
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        chosenReceipes.matchedReceipes.removeAll()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chosenReceipes.matchedReceipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "receipeCell") as! RecipeCell
        
        cell.recipeImage.image = chosenReceipes.matchedReceipes[indexPath.row].image
        cell.recipeTitleLabel.text = chosenReceipes.matchedReceipes[indexPath.row].title
        cell.recipeImage.layer.cornerRadius = cell.recipeImage.frame.height / 2
        cell.recipeImage.layer.masksToBounds = true
        cell.recipeTitleLabel.font = .systemFont(ofSize: 24)
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chosenReceipe = chosenReceipes.matchedReceipes[indexPath.row]
        performSegue(withIdentifier: Constants.recipesDetailsSegueId, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.recipesDetailsSegueId {
            let destinationViewController = segue.destination as? ReceipeDetailsViewController
            destinationViewController?.chosenRecipe = chosenReceipe
        }
    }

}

private extension ReceipesListViewController {
    
    func matchReceipesWithChosenIngredient() {
        receipesList
            .receipes
            .forEach { receipe in
                guard let ingredients = receipe.ingredients else { return }
                
                ingredients.forEach { [weak self] ingredient in
                    guard let self = self else { return }
                    
                    if (ingredient.rawValue == self.chosenIngredient.ingredientName?.rawValue) {
                        self.chosenReceipes.matchedReceipes.append(receipe)
                    }
                }
        }
    }
}
