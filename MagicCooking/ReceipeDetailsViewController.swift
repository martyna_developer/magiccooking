//
//  ReceipeDetailsViewController.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 02/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class ReceipeDetailsViewController: UIViewController {
    
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var ingredientsTitleLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var recipeStepsLabel: UILabel!
    @IBOutlet weak var recipeDescriptionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var chosenRecipe = ReceipeModel()
    
    //ingredientsLabel is for gramatures from RecipeModel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        recipeStepsLabel.text = "Sposób przygotowania"
        recipeStepsLabel.font = .boldSystemFont(ofSize: 15)
        ingredientsTitleLabel.text = "Składniki"
        ingredientsTitleLabel.font = .boldSystemFont(ofSize: 15)
        recipeImage.image = chosenRecipe.image
        recipeTitleLabel.text = chosenRecipe.title
        recipeTitleLabel.font = .boldSystemFont(ofSize: 17)
        ingredientsLabel.text = chosenRecipe.gramatures
        ingredientsLabel.font = .systemFont(ofSize: 15)
        recipeDescriptionLabel.text = chosenRecipe.description
        recipeDescriptionLabel.font = .systemFont(ofSize: 15)
        self.title = chosenRecipe.title
    }
    
}
