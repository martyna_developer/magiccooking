//
//  IngredientsListViewController.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 02/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import UIKit

class IngredientsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private enum Constants {
        static let recipesSegueId = "showRecipes"
    }
    
    lazy var ingredientsList = IngredientsListModel()
    var chosenIngredient = IngredientModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.title = "Magic Cooking"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredientsList.ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ingredientListCell") as! IngredientCell
        
        cell.ingredientImage.image = ingredientsList.ingredients[indexPath.row].ingredientImage
        cell.ingredientTitleLabel.text = ingredientsList.ingredients[indexPath.row].ingredientName.map { $0.rawValue }
        cell.ingredientImage.layer.cornerRadius = cell.ingredientImage.frame.height / 2
        cell.ingredientImage.layer.masksToBounds = true
        cell.ingredientTitleLabel.font = .systemFont(ofSize: 26)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chosenIngredient = ingredientsList.ingredients[indexPath.row]
        performSegue(withIdentifier: Constants.recipesSegueId, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.recipesSegueId {
            let destinationViewController = segue.destination as? ReceipesListViewController
            destinationViewController?.chosenIngredient = chosenIngredient
        }
    }
    
}
