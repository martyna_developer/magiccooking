//
//  IngredientsListModel.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 07/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class IngredientsListModel {
        
        var ingredients: [IngredientModel] = [
            IngredientModel(ingredientName: .bazylia, ingredientImage: UIImage(named: "bazylia")),
            IngredientModel(ingredientName: .chorizo, ingredientImage: UIImage(named: "chorizo")),
            IngredientModel(ingredientName: .czosnek, ingredientImage: UIImage(named: "czosnek")),
            IngredientModel(ingredientName: .drozdze, ingredientImage: UIImage(named: "drozdze")),
            IngredientModel(ingredientName: .jajko, ingredientImage: UIImage(named: "jajko")),
            IngredientModel(ingredientName: .kurczak, ingredientImage: UIImage(named: "kurczak")),
            IngredientModel(ingredientName: .majonez, ingredientImage: UIImage(named: "majonez")),
            IngredientModel(ingredientName: .maka, ingredientImage: UIImage(named: "maka")),
            IngredientModel(ingredientName: .maslo, ingredientImage: UIImage(named: "maslo")),
            IngredientModel(ingredientName: .mozarella, ingredientImage: UIImage(named: "mozarella")),
            IngredientModel(ingredientName: .mleko, ingredientImage: UIImage(named: "mleko")),
            IngredientModel(ingredientName: .olej, ingredientImage: UIImage(named: "olej")),
            IngredientModel(ingredientName: .oliwki, ingredientImage: UIImage(named: "oliwki")),
            IngredientModel(ingredientName: .parmezan, ingredientImage: UIImage(named: "parmezan")),
            IngredientModel(ingredientName: .pieczarki, ingredientImage: UIImage(named: "pieczarki")),
            IngredientModel(ingredientName: .pomidory, ingredientImage: UIImage(named: "pomidory")),
            IngredientModel(ingredientName: .przyprawy, ingredientImage: UIImage(named: "przyprawy")),
            IngredientModel(ingredientName: .ser, ingredientImage: UIImage(named: "ser")),
            IngredientModel(ingredientName: .wolowina, ingredientImage: UIImage(named: "wolowina"))
    ]
    
}
