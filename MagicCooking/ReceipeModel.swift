//
//  ReceipeModel.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 07/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class ReceipeModel {
    
    var id: String?
    var image: UIImage?
    var title: String?
    var gramatures: String?
    var description: String?
    var ingredients: [Ingredient]?

    init(id: String? = nil, image: UIImage? = nil, title: String? = nil, gramatures: String? = nil, description: String? = nil, ingredients: [Ingredient]? = nil) {
        self.id = id
        self.image = image
        self.title = title
        self.gramatures = gramatures
        self.description = description
        self.ingredients = ingredients
    }
    
}
