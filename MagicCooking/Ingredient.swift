//
//  Ingredient.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 07/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

enum Ingredient: String {
    case mozarella = "mozarella"
    case bazylia = "bazylia"
    case czosnek = "czosnek"
    case chorizo = "chorizo"
    case majonez = "majonez"
    case drozdze = "drożdże"
    case jajko = "jajko"
    case maka = "mąka"
    case maslo = "masło"
    case mleko = "mleko"
    case olej = "olej"
    case oliwki = "oliwki zielone"
    case parmezan = "parmezan"
    case pieczarki = "pieczarki"
    case kurczak = "kurczak"
    case pomidory = "pomidory"
    case ser = "ser żółty"
    case wolowina = "wołowina"
    case przyprawy = "sól i pieprz"
}
