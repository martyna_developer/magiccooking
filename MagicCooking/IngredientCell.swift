//
//  IngredientCell.swift
//  MagicCooking
//
//  Created by Martyna Wiśnik on 07/10/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class IngredientCell: UITableViewCell {
    
    @IBOutlet weak var ingredientImage: UIImageView!
    @IBOutlet weak var ingredientTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
